#! /bin/bash -f
source ../clean-setup.sh
# $lf1="BBS-list"
# $lf2="inventory_shipped"

echo ""
# AWK pattern that matches every input record where that text is present
echo "Regex between forward slashes (\"/\")"
echo "awk '/foo/ { print \$0 }' $lf1"
awk '/foo/ { print $0 }' $lf1

echo ""
# AWK pattern that matches the 2nd column of data in every input 
# record where that text is present
echo "awk '/foo/ { print \$2 }' $lf1"
awk '/foo/ { print $2 }' $lf1


echo ""
# Regex matches on (~) or doesn't match on (!~) a pattern
echo "awk '\$1 ~ /J/' inventory_shipped"
awk '$1 ~ /J/' inventory_shipped
echo ""
echo "same as..."
echo "awk '{ if (\$1 ~ /J/) print }' inventory_shipped"
awk '{ if ($1 ~ /J/) print }' inventory_shipped

echo ""
echo "awk '\$1 !~ /J/' inventory_shipped"
awk '$1 !~ /J/' inventory_shipped


## Operators (Metacharacters)
# 
# \              - suppress the special meaning of other Operators (see escape sequences)
# ^              - a string's boundary (beginning)
# $              - a string's boundary (end)
# .              - any character
# [  ...  ]      - any of the characters inside the brackets
# [^ ...  ]      - none of the characters inside of the brackets
# [: ... :]      - a character class; a predefined list of characters
#   [:alnum:]    - any alpha-numeric
#   [:alpha:]    - any alphabetic character
#   [:lower:]    - any lower-case alphabetic character
#   [:upper:]    - any upper-case alphabetic character
#   [:blank:]    - any space or tab character
#   [:digit:]    - any numeric character
#   [:space:]    - any space character (including spaces, tabs, formfeeds, and more)
#   [:punct:]    - any character that isn't a letter, digit, control character, or space character
#   [:xdigit:]   - any character that is a hexadecimal digit
#   [:cntrl:]    - any control character
#   [:print:]    - any character that is not a control character
#   [:graph:]    - any character that is printable and visible (spaces are not visible)
# [. ... .]      - a collating class (Not usable in GAWK because of POSIX English)
# [= ... =]      - an equivalence class (Not usable in GAWK because of POSIX English)
# (  ...  )      - a group of characters, list(s) of characters, or other groups
# ?              - zero or one of a character, list(s) of characters, or groups
# +              - one or more of a character, list(s) of characters, or groups
# *              - zero or more of a character, list(s) of characters, or groups
# ...{n}         - exactly this number of characters, list(s) of characters, or groups
# ...{n,}        - at least this number characters, list(s) of characters, or groups
# ...{n,m}       - between these numbers of characters, list(s) of characters, or groups
# |              - Alternator/Or (if not left side, try the right side)


## Escape Sequences
#
# All of AWK - POSIX
# \\             - outputs a literal backslash
# \/             - outputs a literal forward slash
# \"             - outputs a literal quotation mark
# \a             - outputs an alert
# \b             - outputs a backspace
# \f             - outputs formfeed
# \n             - outputs a new line
# \r             - returns the carriage
# \t             - outputs a horizontal tab
# \v             - outputs a vertical tab
# \000 - \777    - converts a 3 digit octal value (digits 0-7)
# \x00 - \xFF    - converts a 2 digit hexadecimal value (digits 0-F)
#
# GAWK only (All of AWK plus these)
# \               - a string's boundary (beginning... same as "^")
# \               - a string's boundary (end... same as "$")
# \w              - any alpha-numeric (includind the underscore "_")
# \W              - any non alpha-numeric
# \<              - a word's boundary (beginning)
# \>              - a word's boundary (end)
# \y              - a word's boundary (either beginning or end) '\yballs?\y' will match 'ball' or 'balls'
# \B              - a word's boundary (inside the word)         '/\Brat\B/' will match 'crate' but not 'dirty rat'
#
# Command line controls
# no options      - default
# --posix         - Only POSIX regex; no GAWK escape sequences
# --traditional   - POSIX escape sequences without character classes and interval expressions
# --re-interval   - `--traditional` but with interval expressions


echo ""
# AWK regex does not see upper case and lower case characters as the same
echo "AWK regex is case sensitive"
echo "x = \"aB\""
echo "if (x ~ /ab/) ... [FAIL]"
echo ""
echo "IGNORECASE=1"
echo "if (x ~ /ab/) ... [SUCCESS]"


echo ""
# AWK regex will interpret all four 'a's as 'one or more of' with the '/a+/'
# and thus group them all as one and replace all four with a single '<A>'
echo "echo aaaabcd | awk '{ sub(/a+/, \"<A>\"); print }'"
echo aaaabcd | awk '{ sub(/a+/, "<A>"); print }'


rm $lf1 $lf2
