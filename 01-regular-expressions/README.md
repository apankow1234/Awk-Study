# AWK Regular Expressions

Getting Started

```bash
../clean-setup.sh
```

* [Regular Expressions](#regular-expressions)
* [Regex Anatomy](#regex-anatomy)
* [Case Sensitivity](#case-sensitivity)
* [Operators (Metacharacters)](#operators-metacharacters-)
* [Escape Sequences](#escape-sequences)
* [IMPORTANT: AWK Scans Twice](#important-awk-scans-twice)


## Regular Expressions

Regular Expressions (regex) match patterns between two forward slashes ("/"). 

**$0** represents the entire line and **$1**-_$N_ represent columns of data from the inputs.

[BBS-list](../README.md)

```bash
awk '/foo/ { print $0 }' BBS-list
```

> Output
> 
> ```
> fooey        555-1234     2400/1200/300     B
> foot         555-6699     1200/300          B
> macfoo       555-6480     1200/300          A
> sabafoo      555-2127     1200/300          C
> ```

```bash
awk '/foo/ { print $2 }' BBS-list
```

> Output
> 
> ```
> 555-1234
> 555-6699
> 555-6480
> 555-2127
> ```

I have used the book, [AWK Language Programming: A User's Guide for GNU AWK](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_toc.html "autodidactism"), as a source of and inspiration for the examples in this repository.

## Regex Anatomy

Expression that returns records with matches

```
input ~ /regex/
/regex/ ~ input
```

Expression that returns records that do not match

```
input !~ /regex/
/regex/ !~ input
```

### Matching on and not-matching on. 

[inventory_shipped](../README.md)

```bash
awk '$1 ~ /J/' inventory_shipped
```
The expression will output a match and so is the same as requesting the print functionality.

```bash
awk '{ if ($1 ~ /J/) print }' inventory_shipped
```

> Outout _( for both )_
> 
> ```
> Jan  13  25  15 115
> Jun  31  42  75 492
> Jul  24  34  67 436
> Jan  21  36  64 620
> ```

```bash
awk '$1 !~ /J/' inventory_shipped
```

> Outout
> 
> ```
> Feb  15  32  24 226
> Mar  15  24  34 228
> Apr  31  52  63 420
> May  16  34  29 208
> Aug  15  34  47 316
> Sep  13  55  37 277
> Oct  29  54  68 525
> Nov  20  87  82 577
> Dec  17  35  61 401
> 
> Feb  26  58  80 652
> Mar  24  75  70 495
> Apr  21  70  74 514
> ```


## Case Sensitivity 

AWK is case sensitive; upper-case letters are not the same as lower-case.

This will fail

```awk
x = "aB"
if (x ~ /ab/) ...
```

This will succeed

```awk
x = "aB"
IGNORECASE=1
if (x ~ /ab/) ...
```


## Operators (Metacharacters)

Regular expressions allow us to describe a pattern or recurring patterns without needing to type them out explicitly.

These Operators have special meanings.

* `\`... - suppress the special meaning of other Operators (see [Escape Sequences](#Escape-Sequences) up next)
* `^`... - a string's boundary (beginning)
* ...`$` - a string's boundary (end)
* `.` - any character
* `[`...`]` - any of the characters inside the brackets
  * `[^`...`]` - none of the characters inside of the brackets
  * `[:`...`:]` - a character class; a predefined list of characters
    * `[:alnum:]` - any alpha-numeric
    * `[:alpha:]` - any alphabetic character
    * `[:lower:]` - any lower-case alphabetic character
    * `[:upper:]` - any upper-case alphabetic character
    * `[:blank:]` - any space or tab character
    * `[:digit:]` - any numeric character
    * `[:space:]` - any space character (including spaces, tabs, formfeeds, and more)
    * `[:punct:]` - any character that isn't a letter, digit, control character, or space character
    * `[:xdigit:]` - any character that is a hexadecimal digit
    * `[:cntrl:]` - any control character
    * `[:print:]` - any character that is not a control character
    * `[:graph:]` - any character that is printable and visible (spaces are not visible)
  * `[.`...`.]` - a collating class (Not available in GAWK because of POSIX English)
  * `[=`...`=]` - an equivalence class (Not available in GAWK because of POSIX English)
* `(`...`)` - a group of characters, list(s) of characters, or other groups
* ...`?` - zero or one of a character, list(s) of characters, or groups
* ...`+` - one or more of a character, list(s) of characters, or groups
* ...`*` - zero or more of a character, list(s) of characters, or groups
* Interval expressions
  * ...`{`###`}` - exactly this number of characters, list(s) of characters, or groups
  * ...`{`###`,}` - a minimum of this number characters, list(s) of characters, or groups
  * ...`{`###`,`###`}` - between these numbers of characters, list(s) of characters, or groups
* ...`|`... - Alternator/Or (if nothing from the expression on the _left_ side, try the expression on _right_ side)


### A simple example
where four **a**'s are input. The expression `/a+/` is interpreted as 'one or more of **a**' and so all four **a**'s are grouped as a single item; `sub()` will replace all four **a**'s with just one **&lt;A&gt;**

```bash
echo aaaabcd | awk '{ sub(/a+/, "<A>"); print }'
```

> Output
> 
> ```
> <A>bcd
> ```


## Escape Sequences

Along with the above Operators that describe patterns, AWK uses special combinations of character for other patterns. 
* These combinations start with a back slash, `\`, to distinguish them from other letters/words

If we wanted to actually print out one of these patterns, we would have to "escape" it's pattern-meaning to convert it back to text... With another `\` in front.
* `/^key/`  --> only a line where "key" are the first letters
* `/\^key/` --> any line with "^key" anwhere in it
* `/\n/`  --> outputs a `newline`
* `/\\n/` --> outputs "\n"

### All of AWK - POSIX

* `\\` - outputs a literal backslash
* `\/` - outputs a literal forward slash
* `\"` - outputs a literal quotation mark
* `\a` - outputs an alert
* `\b` - outputs a backspace
* `\f` - outputs formfeed
* `\n` - outputs a new line
* `\r` - returns the carriage
* `\t` - outputs a horizontal tab
* `\v` - outputs a vertical tab
* `\000 - \777` - converts a 3 digit octal value
* `\x00 - \xFF` - converts a 2 digit hexadecimal value

### GAWK only (All of AWK plus these)

* ``\` `` - a string's boundary (beginning... same as "^")
* `\' ` - a string's boundary (end... same as "$")
* `\w ` - any alpha-numeric (includind the underscore "_")
* `\W ` - any non alpha-numeric
* `\< ` - a word's boundary (beginning)
* `\> ` - a word's boundary (end)
* `\y ` - a word's boundary (either beginning or end)
  * `'\yballs?\y'` will match 'ball' or 'balls'
* `\B ` - a word's boundary (inside the word)
  * `'/\Brat\B/'` will match 'crate' but not 'dirty rat'

### Command line controls

* no options - Default to all GAWK regex
* `--posix` - Only POSIX regex; no GAWK escape sequences
* `--traditional` - POSIX regex with escape sequences but no character lists nor interval expressions
* `--re-interval` - `--traditional` but with interval expressions

## IMPORTANT: AWK Scans Twice

AWK will scan the entire input at first and then scan each record as you go. Each scan performs a clean-up.

During the scan of the whole input, AWK will ignore cleaning constant regular expressions (between forward slashes, `/`).

Scans:
1. INPUT: skips escape characters in the forward slashes, `/` ... `/`, as it parses the whole input
  * `/\*/` --> `/\*/`
2. RECORD: removes escape characters again as it parses the specific record
  * `/\*/` --> literal asterisk `"*"`

Regular expressions do not have to be constant. You can store a regular expression in a variable and use it in a comparison later.

```awk
BEGIN { identifier_regexp = "[A-Za-z_][A-Za-z_0-9]+" }
$0 ~ identifier_regexp { print }
```

> Notice that the regex variable did not use forward slashes.
> 
> If you need escape characters, you need to escape any escape characters in your variables.
> 
> `/\*/` is the same as `"\\*"` not `"\*"`


Scans with Regular Expression variable:
1. INPUT: escapes escape characters between quotation marks
  * `"\\*"` --> `"\*"`
2. RECORD: escapes escape characters between quotation marks
  * `"\*"` --> literal asterisk `"*"`

Or...
1. INPUT: escapes escape characters between quotation marks
  * `"\*"` --> `"*"`
2. RECORD: escapes escape characters between quotation marks
  * `"*"` (zero or more of what comes before it) --> literal nothingness `""`

