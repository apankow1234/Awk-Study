#! /bin/bash -f
source ../clean-setup.sh
# $lf1="BBS-list"
# $lf2="inventory_shipped"


# AWK call anatomy
#
# awk 'program'
# awk 'program' input-file1
# awk 'program' input-file1 input-file2 ...
# awk -f program-file
# awk -f program-file input-file1
# awk -f program-file input-file1 input-file2 ...

# AWK program anatomy
#
# pattern { action } ...
# pattern { action } pattern { action } ...


echo ""
# single line, no input AWK program
echo "First AWK program: One rule and no input"
echo "awk \"BEGIN {print \\\"Don't Panic!\\\"}\""
#---------------------------------------------------------------------------#
awk "BEGIN {print \"Don't Panic!\"}"
#---------------------------------------------------------------------------#


echo ""
# call an external AWK file program
echo "Call to an external AWK file"
echo "./advice.awk"
cat ./advice.awk | sed "s/[^ ]* */     &/"
#---------------------------------------------------------------------------#
./advice.awk
#---------------------------------------------------------------------------#


echo ""
# simple example with input file and one rule
echo "Simple Example: 1 program rule"
echo "awk '/foo/ { print \$0 }' $lf1"
#---------------------------------------------------------------------------#
awk '/foo/ { print $0 }' $lf1
#---------------------------------------------------------------------------#


echo ""
# two program rules
# every rule is run on every line of every input
# if one pattern matches, one rule is run; if both, both rules are run
# this could lead to duplicates
# $0 represents the current line of the current input
echo "Adding Complexity: 2 program rules"
echo "awk '/12/ { print \$0 } /21/ { print \$0 }' $lf1 $lf2"
#---------------------------------------------------------------------------#
awk '/12/ { print $0 } /21/ { print $0 }' $lf1 $lf2
#---------------------------------------------------------------------------#


echo ""
# intro to variables... they exist
echo "More Complex: Variables are a thing."
echo "ls -lg | awk '\$6 == \"Jan\" { sum += \$5 } END { print sum }'"
#---------------------------------------------------------------------------#
ls -l | awk '$6 == "Jan" { sum += $5 } END { print sum }'
#---------------------------------------------------------------------------#


# AWK programs can be split up onto multiple lines if too long with escape
# back slashes. Different kicks for differnt kids... C vs Bourne Again shells
# do things differently


# AWK has built-in (template/global) variables for use already.
# http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_11.html#SEC108
#---------------------------------------------------------------------------#
# Control Variables
#---------------------------------------------------------------------------#
# `RS`           - Sets the `Input Record Separator`
# `FS`           - Sets the `Input Field Separator`
#                     Command line setting is `-F`
#                     Turns of `FIELDWIDTHS` until `FIELDWIDTHS` is set again
# `FIELDWIDTHS`  - Tells AWK how many characters each field is instead of a pattern
#                     Turns `FS` off until `FS` is set again
# `OFS`          - Sets the `Output Field Separator`
# `ORS`          - Sets the `Output Record Separator`
# `IGNORECASE`   - Tells AWK how to handle letters
#                     `0` : Upper-case versions of letters are different from lower-case letters versions
#                     `1` : Upper-case versions of letters are the same letters as the lower-case versions
# `OFMT`         - Sets the `Output Format`
# `CONVFMT`      - 
# `SUBSEP`       - 
#---------------------------------------------------------------------------#
# Data Carrying Variables
#---------------------------------------------------------------------------#
# `NR`           - The total number of records
# `FNR`          - The "focused on"/ current record number
# `RT`           - The current record's trigger (The value that `RS` found to separate this record from the last)
#                     Especially useful when `RS` is a dynamic regular expression
# `NF`           - The total number of fields in the current record
# `ARGC`         - "Count" of "Arguments" passed to the AWK program
# `ARGV`         - "Variables" of the "Arguments" passed to the AWK program
# `ARGIND`       - Index of the variable currently being referenced from `ARGV`
# `FILENAME`     - The current input that AWK is reading from
#                     If an actual file, this will be the file's name
#                     If from a stream (piped "`|`" in), this will be `-`
#                     If no source, this will be `""`
# `ENVIRON`      -
# `ERRNO`        - 
# `RLENGTH`      - 
# `RSTART`       - 

rm $lf1 $lf2
