# Getting Started With AWK

Getting Started

```bash
../clean-setup.sh
```

* [AWK Anatomy](#awk-anatomy)
* [Examples](#examples)
* [Statements vs lines](#statements-vs-lines)
* [Predefined (Built-in) Variables](#predefined-built-in-variables)


## AWK Anatomy

Call Anatomy

```
awk 'program'
awk 'program' input-file1
awk 'program' input-file1 input-file2 ...

awk -f program-file
awk -f program-file input-file1
awk -f program-file input-file1 input-file2 ...
```

Program Anatomy

```
pattern { action } ...
pattern { action } pattern { action } ...
```

## Examples
Examples are mostly word for word from the book, [AWK Language Programming: A User's Guide for GNU AWK](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_toc.html "autodidactism").

### Single line, no input AWK program

```bash
awk "BEGIN {print \"Don't Panic!\"}"
```

> *Output*
> 
> ```
> Don't Panic!
> ```

### Calling a AWK program from an external file

> _advice.awk_
> 
> ```awk
> #! /usr/bin/gawk -f
> 
> BEGIN { print "Don't Panic!" }
> ```

```bash
./advice.awk
```

> *Output*
> 
> ```
> Don't Panic!
> ```

### Simple examle with one program and an input file

[BBS-list](../README.md)

```bash
awk '/foo/ { print $0 }' BBS-list
```

> *Output*
> 
> ```
> fooey        555-1234     2400/1200/300     B
> foot         555-6699     1200/300          B
> macfoo       555-6480     1200/300          A
> sabafoo      555-2127     1200/300          C
> ```

### Two programs and any number of input files

Every program is run on every input. If a program matches on a line, it runs. If multiple programs match on a line, each will run; this will result in duplicates.

[BBS-list](../README.md)

[inventory_shipped](..README.md)

```bash
awk '/12/ { print $0 } /21/ { print $0 }' BBS-list inventory_shipped
```

> *Output*
> 
> ```
> aardvark     555-5553     1200/300          B
> alpo-net     555-3412     2400/1200/300     A
> barfly       555-7685     1200/300          A
> bites        555-1675     2400/1200/300     A
> core         555-2912     1200/300          C
> fooey        555-1234     2400/1200/300     B
> foot         555-6699     1200/300          B
> macfoo       555-6480     1200/300          A
> sdace        555-3430     2400/1200/300     A
> sabafoo      555-2127     1200/300          C
> sabafoo      555-2127     1200/300          C
> Jan  21  36  64 620
> Apr  21  70  74 514
> ```

### More complex... variables exist

> This example arbitrarily uses a month (`Jan`) and assumes a directory has files made during that month.

```bash
ls -l | awk '$6 == "Jan" { sum += $5 } END { print sum }'
```

> _(my)_ Output
> 
> ```
> 17067
> ```

## Statements vs lines

Each line typed into the shell technically ends with an invisible `newline` character when you press `Enter/Return`. 

AWK understands `newline` characters as `whitespace` and ignores them entirely but the shell sees `newline` characters as the command to `submit and run`

By typing a slash, `\`, at the end of the line, you're telling the shell to ignore the `newline` character and not submit just yet. 

> **NOTE:** C vs Bourne Again shells do things differently. Different kicks for dliffernt kids... 

An AWK `statement` is the full AWK command whether it's on the first `line` or, using slashes, on many `lines`.

## Predefined (Built-in) Variables

[Described Here](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_11.html#SEC108)

Control Variables

* `RS`           - Sets the `Input Record Separator`
* `FS`           - Sets the `Input Field Separator`; command line setting is `-F`
  * Turns of `FIELDWIDTHS` until `FIELDWIDTHS` is set again
* `FIELDWIDTHS`  - Tells AWK how many characters each field is instead of a pattern
  * Turns `FS` off until `FS` is set again
* `OFS`          - Sets the `Output Field Separator`
* `ORS`          - Sets the `Output Record Separator`
* `IGNORECASE`   - Tells AWK how to handle letters
  * `0` : Upper-case versions of letters are different from lower-case letters versions
  * `1` : Upper-case versions of letters are the same letters as the lower-case versions
* `OFMT`         - Sets the `Output Format`
* `CONVFMT`      - 
* `SUBSEP`       - 

Data Carrying Variables

* `NR`           - The total number of records
* `FNR`          - The "focused on"/ current record number
* `RT`           - The current record's trigger (The value that `RS` found to separate this record from the last)
  * Especially useful when `RS` is a dynamic regular expression
* `NF`           - The total number of fields in the current record
* `ARGC`         - "Count" of "Arguments" passed to the AWK program
* `ARGV`         - "Variables" of the "Arguments" passed to the AWK program
* `ARGIND`       - Index of the variable currently being referenced from `ARGV`
* `FILENAME`     - The current input that AWK is reading from
  * If an actual file, this will be the file's name
  * If from a stream (piped "`|`" in), this will be "`-`"
  * If no source, this will be `""`
* `ENVIRON`      -
* `ERRNO`        - 
* `RLENGTH`      - 
* `RSTART`       - 



