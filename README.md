# AWK Quick Start Guide

Getting Started

```bash
./clean-setup.sh
```

* [Etymology](#etymology)
* [AWK Anatomy](#awk-anatomy)
  * [Calls](#calls)
  * [Programs](#programs)
  * [Inputs](#inputs)
* [Performance](#performance)
  * [Double Scanning](#double-scanning)
  * [AWK vs GREP and SED](#awk-vs-grep-and-sed)
  * [AWK vs Python and Perl](#awk-vs-python-and-perl)
  * [Compared to GAWK](#compared-to-gawk)
* [Data Files for the Examples](#data-files-for-the-examples)


## Etymology

AWK is named for its creators
* **A**ho, Alfred
* **W**einberger, Peter
* **K**ernighan, Brian

"awk" - the shell program

"AWK" - the scripting language

## AWK Pipeline

Input Record --> AWK Program --> Output Record


## AWK Anatomy

### Calls

```
awk 'program'
awk 'program' input-file1
awk 'program' input-file1 input-file2 ...

awk -f program-file
awk -f program-file input-file1
awk -f program-file input-file1 input-file2 ...
```

### Programs

```
pattern { action } ...
pattern { action } pattern { action } ...
```

### Inputs

* Input - Keyboard, String (piped in), or File
  * Record
    * Field
    * Field Separator
  * Record Separator
    * Record Trigger
  * Number of Records (Total)
  * Current Record Number (ID)

```
┌────────────────────────────────────────────────────┐
│------------------------Input-----------------------│
│ ┌────────────────────────────────────────────────┐ │
│ │                                                │ │ Current Record = 1
│ │                      Record                    │ │
│ │                                                │ │
│ └────────────────────────────────────────────────┘ │
│ ┌────────────────────────────────────────────────┐ │
│ │                      Record                    │ │ Current Record is $FNR = 2
│ │  ┌───────┐   ┌───────┐    ┌───────┐            │ │
│ │  │ Field │   │ Field │    │ Field │   ....     │ │
│ │  └───────┘   └───────┘    └───────┘            │ │
│ └────────────────────────────────────────────────┘ │
│                          |                         │ Record Trigger
│                   Record Separator                 │ is always the current
│                          |                         │ Record Separator
│ ┌────────────────────────────────────────────────┐ │
│ │          The Whole Record is Field 0           │ │ $FNR = 3
│ │ ┌─────────┐                   ┌─────────┐      │ │
│ │ │ Field 1 │ -Field Separator- │ Field 2 │ .... │ │
│ │ └─────────┘                   └─────────┘      │ │
│ └────────────────────────────────────────────────┘ │
│                          |                         │
│                         $RS                        │ $RT = $RS
│                          |                         │
│ ┌────────────────────────────────────────────────┐ │
│ │                        $0                      │ │ $FNR = 4
│ │ ┌───────┐       ┌───────┐       ┌───────┐      │ │
│ │ │  $1   │ -$FS- │  $2   │ -$FS- │  $3   │ .... │ │
│ │ └───────┘       └───────┘       └───────┘      │ │
│ └────────────────────────────────────────────────┘ │
│                         ....                       │
└────────────────────────────────────────────────────┘
$NR is the Total number of records
$FNR is the current record ("focus number")
```

## Performance

### Double Scanning

AWK will scan the entire input at first and then scan each record as you go. Each scan performs a clean-up.
* AWK ignores cleaning constant regular expressions (between forward slashes, `/`) until scanning each record.

### AWK vs GREP and SED

Although, not entirely true, we can think of GREP, SED, and AWK as steps up from each other.

GREP - "**G**lobally search a **R**egular **E**xpression and **P**rint"
* From the POSIX regular expression format `g/re/p`
  * the `g/` at the beginning represents `global`
  * the `re` between the forward slashes stands for "regular expressions"
  * the `/p` at the end represents `print`
* Searches just once over the current input

SED - "**S**tream **Ed**itor"
* A complex combination of GREP, "GREP for substitution (`g/re/s`)", and "GREP for deletion (`g/re/d`)"
* Searches just once over the current input
* SED's format follows `s/` ... `/` ... `/` ...
  * the `s/` at the beginning represents `substitute`
  * the first gap between forward slashes if for the search pattern
  * the second gap is for the substitution
    * a blank subsitution is the same as `g/re/d`
  * the place after the slashes is for SED specific commands like `p` for print

AWK
* Even more complex; now considered a full (loosely-typed) language
* Able to search forward and backward over several inputs.
  * Can and does use arrays, variables, and conditions
* Includes functionality similar to C/C++ for ease of use.
* AWK is meant for simple input/output.


### AWK vs Python and Perl

Perl - Meant to sound friendly; not a real acronym
* Invented to purposely outperform AWK
  * Uses SED and AWK features
    * `s2p` - Program to convert SED to Perl
    * `a2p` - Program to convert AWK to Perl
* Provides access to system calls
  * AWK does not
* Allows users to create custom functionality
  * AWK does not

Python - Named after Monty Python
* Invented to be easier to learn than Perl
  * Not much faster than Perl
* Is Object Oriented (the logic is easier to read)
  * AWK is not
  * Perl can be
* Provides access to system calls
  * AWK does not
* Allows users to create custom functionality
  * AWK does not

### Compared to GAWK

> From [Don’t MAWK AWK](https://brenocon.com/blog/2009/09/dont-mawk-awk-the-fastest-and-most-elegant-big-data-munging-language/)
> 
> Code [Here](https://github.com/brendano/awkspeed)
> 
> ```
> Language      Time     Speed    Lines      Comments
>            (min:sec)            of code    
> mawk          1:06     7.8x     3          Not as many features; Byte-code interpreter
> java          1:20     6.4x     32         VM + JIT
> c-ish c++     1:35     5.4x     42         Using stdio
> python        2:15     3.8x     20         
> perl          3:00     2.9x     17         
> c++           6:50     1.3x     4          Using iostream
> ruby          7:30     1.1x     22         
> gawk          8:35     1x       3          Supports i18n
> ```

## Data Files for the Examples
I have used the book, [AWK Language Programming: A User's Guide for GNU AWK](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_toc.html "autodidactism"), as a source of and inspiration for the examples in this repository.

> 1. _BBS-list_
>   * from [Introduction: Data Files for the Examples](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_2.html#SEC9)
>   * single-line records with information about computer bulletin board systems.
>     1. the name of a computer bulletin board
>     2. its phone number
>     3. the board's baud rate(s)
>     4. a code for the number of hours it is operational
>       * _A_ - the board operates 24 hours a day
>       * _B_ - the board operates evening and weekend hours, only
>       * _C_ - the board operates only on weekends
> 2. *inventory_shipped*
>   * from [Introduction: Data Files for the Examples](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_2.html#SEC9)
>   * single-line records with information about shipments on a monthly basis
>     1. the month of the year
>     2. the number of green crates shipped
>     3. the number of red boxes shipped
>     4. the number of orange bags shipped
>     5. the number of blue packages shipped
> 3. *addresses*
>   * names from [a random generater](http://listofrandomnames.com/index.cfm?generated)
>   * addresses from [a random generater](https://www.randomlists.com/random-addresses)
>   * multi-line records, separated by blank lines
>     1. the person's name
>     2. their street address
>     3. the city
>     4. the state
>     5. their postal code


