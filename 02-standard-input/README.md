# AWK Input

Getting Started

```bash
../clean─setup.sh
```


* [Input Anatomy](#input-anatomy)
* [Splitting Input](#splitting-input)
  * [The Record Separator](#the-record-separator)
  * [The Values of `RS` and `RT`](#the-values-of-rs-and-rt)
  * [The Field Separator](#the-field-separator)
  * [The Values of `FS`](#the-values-of-fs)
  * [Fixed-width Fields (No `FS`)](#fixed-width-fields-no-fs-)
* [Modifying Input Fields](#modifying-input-fields)
  * [AWK Doesn't Affect The Original Input](#awk-doesn-t-affect-the-original-input)
  * [Non-existant Fields](#non-existant-fields)
  * [Making A Field Blank](#making-a-field-blank)
* [Multi-line Records](#multi-line-records)

## Input Anatomy

* Input - Keyboard, String (piped in), or File
  * Record
    * Field
    * Field Separator
  * Record Separator
    * Record Trigger
  * Number of Records (Total)
  * Number of Records (Processed/ Found)

```
┌────────────────────────────────────────────────────┐
│------------------------Input-----------------------│
│ ┌────────────────────────────────────────────────┐ │
│ │                                                │ │ Current Record = 1
│ │                      Record                    │ │
│ │                                                │ │
│ └────────────────────────────────────────────────┘ │
│ ┌────────────────────────────────────────────────┐ │
│ │                      Record                    │ │ Current Record = $FNR = 2
│ │  ┌───────┐   ┌───────┐    ┌───────┐            │ │
│ │  │ Field │   │ Field │    │ Field │   ....     │ │
│ │  └───────┘   └───────┘    └───────┘            │ │
│ └────────────────────────────────────────────────┘ │
│                          |                         │ Record Trigger
│                   Record Separator                 │ is always the current
│                          |                         │ Record Separator
│ ┌────────────────────────────────────────────────┐ │
│ │          The Whole Record is Field 0           │ │ $FNR = 3
│ │ ┌─────────┐                   ┌─────────┐      │ │
│ │ │ Field 1 │ -Field Separator- │ Field 2 │ .... │ │
│ │ └─────────┘                   └─────────┘      │ │
│ └────────────────────────────────────────────────┘ │
│                          |                         │
│                         $RS                        │ $RT = $RS
│                          |                         │
│ ┌────────────────────────────────────────────────┐ │
│ │                        $0                      │ │ $FNR = 4
│ │ ┌───────┐       ┌───────┐       ┌───────┐      │ │
│ │ │  $1   │ -$FS- │  $2   │ -$FS- │  $3   │ .... │ │
│ │ └───────┘       └───────┘       └───────┘      │ │
│ └────────────────────────────────────────────────┘ │
│                         ....                       │
└────────────────────────────────────────────────────┘
$NR is the Total number of records
$FNR is the current record ("focus number")
```

## Splitting Input

### The Record Separator

(default is the `newline` character)

Use the `RS` Predefined Control Variable with an 'equals' (`=`) sign to use a different separator.

```awk
RS = "..."
```

Regardless of the `RS` :
* The beginning of a file will always mark the beginning of a record.
* The end of a file will always mark the end of a record.

#### RS Example

Examples are mostly word for word from the book, [AWK Language Programming: A User's Guide for GNU AWK](http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_toc.html "autodidactism").

`RS = "/"` separates each record by a forward slash. 

`print $0` means to print the entire record line; not a particular field

> By default, lines in files _end_ with a `newline` character; the output will look like records are being split by both `newline` and "/" but they are not.

```bash
awk 'BEGIN { RS = "/" } ; { print $0 }' BBS-list
```

[BBS-list](../README.md)

> Output
> 
> ```
> aardvark     555-5553     1200
> 300          B
> alpo-net     555-3412     2400
> 1200
> 300     A
> barfly       555-7685     1200
> 300          A
> bites        555-1675     2400
> 1200
> 300     A
> camelot      555-0542     300               C
> core         555-2912     1200
> 
>                 ...
> 
> 300     A
> sabafoo      555-2127     1200
> 300          C
> ```

To prove the separator is not including any ending `newline` character, we find just the first fields.

```bash
awk 'BEGIN { RS = "/" } ; { print $1 }' BBS-list
```

> Output
> 
> ```
> aardvark
> 300
> 1200
> 300
> 300
> 1200
> 300
> 
> ...
> 
> 300
> 300
> ```

### The values of RS and RT

`RS` can be set to:

* a single character
* any string of characters
* a regular expression
* `""` - One or more blank lines

The Predefined Variable, `RT`, will be whatever `RS` is set to or searching for.

* If `RS` is a single character or string, `RT` will also be that character or string.
* If `RS` is a regular expression, `RT` will be whichever expression matched.
* If `RS` is blank (`""`), `RT` will be a `newline` character.

#### Example

`RS` is set to a `newline` character _or_ one-or-more upper-case characters
`RT` is set to whichever `RS` finds

```bash
echo record 1 AAAA record 2 BBBB record 3 | \
	gawk 'BEGIN { RS = "\n|( *[[:upper:]]+ *)" } \
	{ print "Record =", $0, "and RT =", RT }'
```

There are 2 instances of upper-case character strings and no explicit `newline` characters.

The end of the record sets both `RS` and `RT` to blank; which _is interpretted_ as a `newline`.

> Output
> 
> ```
> Record = record 1 and RT =  AAAA 
> Record = record 2 and RT =  BBBB 
> Record = record 3 and RT = 
> 
> 
> ```

### The Field Separator

(default is `whitespace`, tab and space characters only)

Use the `FS` Predefined Control Variable with an 'equals' (`=`) sign to use a different separator.

```awk
FS = "..."
```

If in the command line, you set `FS` with `-F`.

```bash
awk -F "..."
```

`FS` in AWK works the same as `IFS` in the terminal (shell, BASH)

#### FS example (in program)

```bash
echo John Q. Smith, 29 Oak St., Walamazoo, MI 42139 | \
	awk 'BEGIN { FS = "," } ; { print $2 }'
```

> Output
> 
> ```
>  29 Oak St.
> ```

#### FS example (command line)

> _baud.awk_
> 
> 
> ```awk
> #! /usr/bin/gawk -f
> 
> /300/ { print $1 }
> ```

```bash
awk -F- -f baud.awk BBS-list
```

> Output
> 
> ```
> aardvark     555
> alpo
> barfly       555
> bites        555
> camelot      555
> core         555
> fooey        555
> foot         555
> macfoo       555
> sdace        555
> sabafoo      555
> ```

### The values of FS

`FS` can be set to:

* a single character
* a regular expression
* `" "` - One or more spaces
* `""` - Blank, no spaces

Rules

* If `FS` is blank, fields are separated by each character
* If `FS` is one or more spaces, the fields are separated by spaces
* If `FS` is any other character or regex, two consecutive occurances (back-to-back/ doubled-up) represent an empty field

#### More FS examples

Default `FS` is a `whitespace` (tabs and spaces) character.

```bash
echo ' a b c d ' | awk '{ print $2 }'
```

Explicitly forcing `FS` to be tabs and spaces.

```bash
echo ' a  b  c  d ' | awk 'BEGIN { FS = "[ \t]+" } { print $2 }'
```

> Output (for both) 
> 
> ```
> a
> ```

Setting `FS` in a dynamic way. 

```bash
echo '   a b c d' | awk '{ print; $2 = $2; print }'
```

Setting a field forces AWK to rescan the input, which strips remaining `whitespace` by default. 

Printing again will let us see this.

> Output

> ```
>    a b c d
> a b c d
> ```

### Fixed width fields (No FS)

Sometimes fields have exactly a certain number of characters. This makes it easy to separate fields without regular expressions or if we need the extra `whitespace`
* Too many characters - Gets interrupted and cut off
* Too few characters - Gets extra spaces to make up the difference

AWK can be told with the Predefined Variable, `FIELDWIDTHS`, how many characters are in each field.

```bash
w -hf | gawk 'BEGIN { FIELDWIDTHS = "9 6 10 6 7 7 35"} { print $1 $3 }'
```

> _(My)_ Output
> 
> ```
> guest        15:35
> andrew       07:59
> ```

**GAWK ONLY**

`FIELDWIDTHS` and `FS` are mutually exclusive in GAWK... using one will stop GAWK from using the other. 

By setting `FIELDWIDTHS` you're telling GAWK to not use `FS`

Setting `FS = FS` turns `FIELDWIDTHS` off and tells GAWK to use `FS` again.

## Modifying Input Fields

### AWK doesn't affect the original input

AWK can be told to read the input, records, and fields differently but will not damage the original content.

```bash
awk '{ $3 = $2 - 10; print $2, $3 }' inventory_shipped
```

> Output
> 
> ```
> 13 3
> 15 5
> 15 5
> 31 21
> 
> ...
> 
>  -10
> 21 11
> 26 16
> 24 14
> 21 11
> ```

Assigning fields to be something else causes AWK to re-create the current record but with your substitution(s) in place of the original.

### Non-existant fields

This includes creating a field that never existed. If you assign a field a value, AWK will re-create the current record and use the newly created version for the output.


```bash
awk '{ $6 = ($5 + $4 + $3 + $2); print $6 }' inventory_shipped
```

> Output
> 
> ```
> 168
> 297
> 301
> 566
> ...
> 0
> 741
> 816
> 664
> 679
> ```

If you ask for a field that doesn't exist, you'll just get an empty string, `""`.

```bash
echo a b c d | awk '{ print $5 }'
```

> Output
> 
> ```
> 
> ```

### Making a field blank

If you had blanks in the original input, they would not become fields because AWK doesn't interpret `whitespace` from an input.

However, once an input is inside AWK, setting a field to be `""` or `''`, simply makes it blank. AWK re-creates the record at hand but already knows about the field's existance from its creation.

```bash
echo a b c d | awk '{ $2=""; print $0 }'
```

> Output
> 
> ```
> a  c d
> ```

## Multi-Line Records

Sometimes records come on multiple lines of the input rather than all on one line. 

`RS = ""` is almost the same as `RS = "\n\n+"`

Both will look for a blank line to separate records by rather than just separating by each line.

* `RS = ""` has AWK remove the beginning and ending `whitespace`
* `RS = "\n\n+"` has AWK keep the beginning and ending `whitespace`

> _addrs.awk_
> 
> ```awk
> BEGIN { RS = "" ; FS = "\n" }
> {
> 	print "Name is:", $1
> 	print "Address is:", $2
> 	print "City and State are:", $3
> 	print ""
> }
> ```

```bash
awk -f addrs.awk addresses
```

> Output
> 
> ```
> Name is: Linsey Hempel
> Address is: 361 Sussex Lane 
> City and State are: Andover, MA 01810
> 
> Name is: Yulanda Henton
> Address is: 950 Rockville Dr. 
> City and State are: Simpsonville, SC 29680
> 
> Name is: Kristie Schranz
> Address is: 42 Brickyard Road 
> City and State are: Round Lake, IL 60073
> 
> ...
> 
> Name is: Serina Amsler
> Address is: 8044 West Rockwell St. 
> City and State are: Logansport, IN 46947
> ```

