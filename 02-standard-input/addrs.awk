#! /usr/bin/gawk -f
# CREDIT: http://kirste.userpage.fu-berlin.de/chemnet/use/info/gawk/gawk_6.html#SEC45

# Records are separated by blank lines.
# Each line is one field.
#--------------------------------------#
BEGIN { RS = "" ; FS = "\n" }
{
	print "Name is:", $1
	print "Address is:", $2
	print "City and State are:", $3
	print ""
}
#--------------------------------------#
