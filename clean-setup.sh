#! /bin/bash -f

if [ ! -f /usr/bin/gawk ]; then
	if [ -f /etc/debian_version ]; then
		sudo apt install -fy -qq gawk
	elif [ -f /etc/redhat-release ]; then
		sudo yum install -q -y gawk
	fi
fi


call_from=`pwd`
get_from="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

example_files=( 
	"BBS-list"
	"inventory_shipped"
	"addresses"
)

for f in ${example_files[@]}; do
	if [ ! -f $call_from/$f ]; then 
		cp $get_from/example_files/$f $call_from/$f; 
		chmod +x $call_from/$f
	fi 
done
